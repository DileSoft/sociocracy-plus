### Регулярный сбор сообщества



Регулярный сбор сообщества или клубное мероприятие - это регулярный вид транслятора. 



Встречи, которые происходят на регулярной основе и служат обменом опыта, общения, обменом ресурсами и само собой основной практике сообщества.



Так как трансляторы разных сообществ могут сильно различаться, то изложим основные задачи, которые ставят участники ядра (команды) перед такими сборами:

- Приоритезация текущего списка задач
- Знакомство с новыми участниками и неформальное общение, поддержка старожилов
- Обсуждение насущных проблем



Важно не путать регулярный сбор с ежедневным стендапом. Регулярный предполагает общение в формате ядро + актив + аура. Ежедневный стендап общение в формате: ядро + актив.