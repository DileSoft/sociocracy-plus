## Ресурсы



**Ресурсы (inputs)** – то, что вам потребуется для реализации проекта, достижения социального результата. Ресурсы могут быть человеческие, материальные, производственные и другие.

