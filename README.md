# Sociocracy Plus

**Авторы и участники внесшие значительный вклад**: [Список авторов и участников внесшие значительный вклад](/contributors.md)

**version:** ***0.1.x***

***CC-BY-SA***

**Sociocracy Plus** - методология управления горизонтальной организацией на основе паттернов из социократии 3.0, опен сорс, методологии неформальной социотехники и деятельного образования.

## Contribute

1. Установите `Typora`
2. Установите `git` 
3. Склонируйте репозиторий командой `git clone https://gitlab.com/protopiahome-public/methodology/sociocracy-plus.git `
4. Откройте любой файл и внесите изменения
5. Сделайте `git commit`
6. Дождитесь подтверждения от mainteiner
7. Ваш вклад стал частью общего дела



##  Build Instructions

With Docker Compose:

```bash
# Site:
$ docker-compose run --rm foliant make site
# PDF:
$ docker-compose run --rm foliant make pdf
```

With pip and stuff (requires Python 3.6+, Pandoc, and TeXLive):

```bash
$ pip install -r requirements.txt
# Site:
$ foliant make site
# PDF:
$ foliant make pdf
```

## Result

- [Скомпилированный сайт](https://protopiahome-public.gitlab.io/methodology/sociocracy-plus/)

## Community

- [Сообщество разработчиков методологий](https://t.me/protopia_framework)
- [Сообщество практиков социократии 3.0](https://t.me/sociocracy30)